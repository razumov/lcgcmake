#!/usr/bin/env python

# Author: Pere Mato
# Usage: lcgcmake [command] [options]
# This command drives the full process of building a LCG release using the 'lcgcmake' tool
# commands
#     configure
#     build
#     install
#     reset
#     show
# optional arguments:
#    -h, --help                       show this help message and exit
#

#----------------------------------------------------------------------------------------------------
from __future__ import print_function
import argparse, sys, os, json, platform, re, tarfile, subprocess, shutil, multiprocessing, string
from distutils.version import LooseVersion

if sys.version_info[0] >= 3 :
  import urllib.request as urllib2
else:
  import urllib2

#---Globals------------------------------------------------------------------------------------------

srcURL = 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources'
binURL = 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases'
ctbURL = 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/contrib'
if os.path.exists(os.path.join(os.getcwd(),'.lcgcmake')) :
  rcPATH = os.path.join(os.getcwd(),'.lcgcmake')
else:
  rcPATH = os.path.join(os.environ['HOME'],'.lcgcmake')
contrib_suffix = ''
gtar_version = '1.30-a85f4'
gfor_version = '6.3.0'
cmake_version = '3.11.1'

config = {}
helpstring = '{0} [sub-command] [options]\n\nThis command drives the full process of building a LCG release using the lcgcmake tool.\n'

#---Bootstrap-----------------------------------------------------------------------------------------
def lcgcmake_bootstrap(compiler, arch, osvers, prefix) :
  environment = {}
  #---Check compiler
  if compiler != 'native' :
    compilers = get_available_compilers(arch, osvers)
    if compiler not in compilers:
      print("Requested compiler %s not found. Use 'lcgcmake show compilers' to list the available compilers" % compiler)
      return environment
    d = compilers[compiler]
    c, v, h = d['NAME'].split('-')[:3]
    compilerpath = os.path.join(prefix, c, v + '-' + h, "-".join([arch,osvers]) + contrib_suffix)
    if not os.path.exists(compilerpath) :
      print('Requested compiler %s not found. Installing it.' % compiler)
      install_tarfile(d['NAME'], arch, osvers, contrib_suffix, prefix)
      for dep in d['DEPENDS']:
        p, v, h = dep.split('-')[:3]
        packagepath = os.path.join(prefix, p, v + '-' + h, "-".join([arch,osvers]) + contrib_suffix)
        if not os.path.exists(packagepath) :
          print('Dependent package %s-%s not found. Installing it.' % (p,v))
          install_tarfile(dep, arch, osvers, contrib_suffix, prefix)
        else:
          print('Dependent package %s-%s already exists.' % (p,v))
    else:
      print('Compiler %s already installed' % compiler)
    #---define environment for compiler
    for dep in  d['DEPENDS']:
      p, v, h = dep.split('-')[:3]
      packagepath = os.path.join(prefix, p, v + '-' + h, "-".join([arch,osvers]) + contrib_suffix)
      set_env_variable(environment, 'PATH', os.path.join(packagepath,'bin'))
      set_env_variable(environment, 'LD_LIBRARY_PATH', os.path.join(packagepath, p in ('gcc',) and 'lib64' or 'lib'))
      if p == 'gcc' :  
        set_env_variable(environment, 'FC', os.path.join(packagepath,'bin','gfortran'))
        set_env_variable(environment, 'COMPILER_PATH', packagepath)
    set_env_variable(environment, 'PATH', os.path.join(compilerpath,'bin'))
    set_env_variable(environment, 'LD_LIBRARY_PATH', os.path.join(compilerpath, c in ('gcc',) and 'lib64' or 'lib'))
    set_env_variable(environment, 'CC', os.path.join(compilerpath,'bin',c))
    set_env_variable(environment, 'CXX', os.path.join(compilerpath,'bin', c in ('gcc',) and 'g++' or (c + '++')))
    if c == 'gcc' : 
      set_env_variable(environment, 'FC', os.path.join(compilerpath,'bin','gfortran'))
  else:
    if platform.system() == 'Darwin':
      set_env_variable(environment, 'CC', shutil.which('clang'))
      set_env_variable(environment, 'CXX', shutil.which('clang++'))
    print('using native compiler')
  set_env_variable(environment, 'COMPILER',compiler)

  if platform.system() == 'Darwin':
    gtarpath = os.path.join(prefix, 'gtar', gtar_version, "-".join([arch,osvers]) + '-clang91-opt')
    if not os.path.exists(gtarpath) :
      print('Installing gnu-tar version %s' % gtar_version)
      install_tarfile('gtar' + '-' + gtar_version, arch, osvers,'-clang91-opt', prefix)
    set_env_variable(environment, 'PATH', os.path.join(gtarpath,'bin'))

    if shutil.which('gfortran') is None:
      fcpath = os.path.join(prefix, 'gfortran', gfor_version, "-".join([arch,osvers]))
      if not os.path.exists(fcpath) :
        print('Installing gfortran version %s' % gfor_version)
        install_tarfile('gfortran' + '-' + gfor_version, arch, osvers, contrib_suffix, prefix)
      set_env_variable(environment, 'PATH', os.path.join(fcpath,'bin'))

  return environment


#---Configure-----------------------------------------------------------------------------------------
def lcgcmake_configure(args) :
  #---setup some variables with arguments and defaults
  environment = {}
  config = {}
  sourcedir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
  
  #---read the old configuration if exists
  global rcPATH
  if args.local:
    rcPATH = os.path.join(os.getcwd(),'.lcgcmake')  # overwrite the location of .lcgcmake
  if os.path.exists(os.path.join(rcPATH,'config.json')):
    config = json.load(open(os.path.join(rcPATH,'config.json'),'r'))

  compiler = args.compiler or config.get('COMPILER', 'native')
  version = args.version or config.get('VERSION', get_default_version())
  prefix = args.prefix or config.get('PREFIX', '/opt/lcg')
  lcg_prefix = args.lcg_prefix or config.get('LCG_PREFIX', None)
  toolchain = args.toolchain or config.get('TOOLCHAIN', None)
  options = args.options or config.get('OPTIONS', [])
  pyversion = args.pyversion or config.get('PYVERSION',str(sys.version_info[0]))
  target_platform = args.target_platform or config.get('BINARY_TAG', None)
  build_always = args.build_always if args.build_always is not None else config.get('BUILD_ALWAYS', False) 
  debug = args.debug
  arch = args.arch
  osvers = args.osvers

  #---deal with the installation prefix
  prefix = os.path.realpath(prefix)
  if not os.path.exists(prefix) :
    d = os.path.dirname(prefix)
    while d != '/' :
      if os.path.exists(d) :
        if os.access(d, os.W_OK):
          break
        else:
          print("You have no access to directory '%s' for writing. Change prefix '%s' to a writable area." % (d, prefix))
          sys.exit(1)
      d = os.path.dirname(d)

  #---deal with cmake and its minimal version
  cmake = subprocess.check_output(['/bin/bash','-l', '-c','which cmake']).decode('utf-8').strip().split('\n')[-1].strip()
  cmake_vers = subprocess.check_output([cmake,'--version']).decode('utf-8').strip().split('\n')[0].split()[-1]
  if LooseVersion(cmake_vers) < LooseVersion("3.2"):
    cmakename = "-".join(['cmake', cmake_version, platform.system(), arch])
    cmakepath = os.path.join(prefix, cmakename)
    if not os.path.exists(cmakepath) :
      print('Installing CMake version %s' % cmake_version)
      resp = urllib2.urlopen(os.path.join(binURL, cmakename + '.tar.gz'))
      tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
      tar.extractall(path=prefix)
    if platform.system() == 'Darwin':
      cmake = os.path.join(cmakepath,'CMake.app','Contents','bin','cmake')
    else:
      cmake = os.path.join(cmakepath,'bin','cmake')

  #---deal with new compiler
  if 'COMPILER' not in config or config['COMPILER'] != compiler:
    # need to setup a new compiler
    environment = lcgcmake_bootstrap(compiler, arch, osvers, prefix)
    if not environment : sys.exit(1)
    # need to delete caches
    if os.path.exists('CMakeCache.txt') : os.remove('CMakeCache.txt')
    if os.path.exists('CMakeFiles') : shutil.rmtree('CMakeFiles')
    if os.path.exists('bintarlisting.txt') : os.remove('bintarlisting.txt')
  else:
    # recover the environment from previous run
    environment = config['ENVIRONMENT']
  
  #---execute cmake to configure
  c_options = [
    '-DLCG_VERSION=%s' % version, 
    '-DCMAKE_INSTALL_PREFIX=%s' % prefix,
    '-DLCG_SOURCE_INSTALL=OFF',
    '-DLCG_PYTHON_VERSION=%s' % pyversion,
    '-DLCG_ADDITIONAL_REPOS=http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/latest'
  ]
  if(args.binary):
    c_options += ['-DUSE_BINARIES=ON']
  else:
    c_options += ['-DUSE_BINARIES=OFF']

  if(args.withtarfiles):
    c_options += ['-DLCG_TARBALL_INSTALL=ON']
  else:
    c_options += ['-DLCG_TARBALL_INSTALL=OFF']

  if(args.withrpms):
    print("Option --with-rmps not yet implemented!!")

  if(debug):
    c_options += ['-DCMAKE_BUILD_TYPE=Debug']

  if(target_platform):
    c_options += ['-DBINARY_TAG=' + target_platform]

  if(toolchain):
    c_options += ['-DLCG_TOOLCHAIN_FILE=' + toolchain]

  if(build_always):
    c_options += ['-DLCG_BUILD_ALWAYS=ON']
  else:
    c_options += ['-DLCG_BUILD_ALWAYS=OFF']

  if(lcg_prefix):
    c_options += ['-DLCG_INSTALL_PREFIX=' + lcg_prefix]

  if(options) :
    for opt in options:
      c_options += ['-D%s' % opt]

  setup_environment(environment)
  rc = subprocess.call([cmake, sourcedir] + c_options)
  if rc != 0 :
    print('Configuration has failed. Please correct the problem guided by the error messages and try again')
    sys.exit(rc)

  lcgplatform = subprocess.check_output(['grep','LCG_platform:STRING=','CMakeCache.txt']).strip()[20:].decode('utf-8')

  #---link compiler and dependent packages (assuming that PATH in the environment has everything needed)
  for pkg in environment.get('PATH', []):
    n, v, p = pkg.split(os.sep)[-4:-1]
    dest = os.path.join(prefix, version, n, v.split('-')[0])
    if not os.path.exists(dest) :  os.makedirs(dest)
    if not os.path.islink(os.path.join(dest,lcgplatform)) : os.symlink(os.path.dirname(pkg), os.path.join(dest, lcgplatform))

  #---save configuration
  config['COMPILER'] = compiler
  config['PREFIX'] = prefix
  config['ARCH'] = arch
  config['OSVERS'] = osvers
  config['ENVIRONMENT'] = environment
  config['SOURCEDIR'] = sourcedir 
  config['CMAKE'] = cmake
  config['VERSION'] = version
  config['PLATFORM'] = lcgplatform
  config['OPTIONS'] = options
  config['PYVERSION'] = pyversion
  config['LCG_PREFIX'] = lcg_prefix
  config['TOOLCHAIN'] = toolchain
  config['BUILD_ALWAYS'] = build_always

  if not os.path.exists(rcPATH) : os.mkdir(rcPATH)
  json.dump(config, open(os.path.join(rcPATH,'config.json'),'w'), sort_keys=True, indent=2, separators=(',', ': '))


#---Install-----------------------------------------------------------------------------------------
def lcgcmake_install(args) :
  if not os.path.exists(os.path.join(rcPATH,'config.json')):
    print("lcgcmake session has not been configured. Use the command 'lcgcmake configure' first.")
    sys.exit(1)
  if not os.path.exists('CMakeCache.txt') :
    print("It seems you are not in the right build area where lcgcmake was configured. Use the command 'lcgcmake configure' first.")
    sys.exit(1)
  targets = args.targets

  #---read configuration
  config = json.load(open(os.path.join(rcPATH,'config.json'),'r'))
  setup_environment(config['ENVIRONMENT'])

  rc = subprocess.call(['make', '-j%d' % (multiprocessing.cpu_count())] + targets)
  if rc == 0 :
    rc = subprocess.call(['make','view'])
    if rc == 0 :
      if platform.system() == 'Darwin' and 'mac' in config['PLATFORM']:
        print('Running MacOS relocation of binary objects ...')
        subprocess.call([os.path.join(config['SOURCEDIR'],'cmake','scripts','fix_mac_relocation.py'),os.path.join(config['PREFIX'],config['VERSION'],config['PLATFORM'])])
      print("Installation of '%s' has been successful." % (' '.join(targets),))
      print("  To set the running environment do 'source %s/%s/%s/setup.sh" % (config['PREFIX'],config['VERSION'],config['PLATFORM']))
      print("  Or run the command 'lcgcmake run [command]'")
    else:
      sys.exit(rc)
  else:
    sys.exit(rc)

#---Show------------------------------------------------------------------------------------------------
def lcgcmake_show(args) :
  if args.what == 'config':
     if not os.path.exists(os.path.join(rcPATH,'config.json')):
       print("lcgcmake session has not been configured. Use the command 'lcgcmake configure' first.")
     else:
       print('Current configuration:')
       print(open(os.path.join(rcPATH,'config.json'),'r').read())
  elif args.what == 'targets':
    config = json.load(open(os.path.join(rcPATH,'config.json'),'r'))
    targets = subprocess.check_output([config['CMAKE'], '--build', '.', '--target', 'help']).decode('utf-8')
    # start from '... clean'
    i = targets.find('... clean')
    targets = [x for x in targets[i:].split() if x not in ('...') and 'clean-' not in x and '-dependencies' not in x]
    targets.sort()
    stargets=[]
    last=None
    for t in targets:
      if '-' in t : stargets[-1].append(t)
      else: stargets.append([t])
    for t in stargets:
      print(','.join(t))
  elif args.what == 'compilers':
    compilers = get_available_compilers(args.arch, args.osvers)
    for c,v in compilers.items():
      print('%s = %s' % (c,'-'.join((v['NAME'].split('-')[:2]))))
    print('native = %s' % get_native_compiler()) 
  elif args.what == 'versions':
    for v in get_available_versions(): print(v)
  elif args.what == 'packages':
    for v in  get_defined_packages(): print(v)

#---Run-------------------------------------------------------------------------------------------------
def lcgcmake_run(args):
  if not os.path.exists(os.path.join(rcPATH,'config.json')):
    print("lcgcmake session has not been configured. Use the command 'lcgcmake configure' first.")
    sys.exit(1)
  #---read configuration
  config = json.load(open(os.path.join(rcPATH,'config.json'),'r'))

  setup = '%s/%s/%s/setup.sh' % (config['PREFIX'],config['VERSION'],config['PLATFORM'])
  rc = subprocess.call(['/bin/bash', '-i', '-c','export PS1=[lcgcmake\ \\\W]\$\  && source '+ setup + ' &&' + ' '.join(args.runcmd)])

#---Version-------------------------------------------------------------------------------------------------
def lcgcmake_version(args):
  print('0.1')

#---Reset-------------------------------------------------------------------------------------------------
def lcgcmake_reset(args):
  #---read the old configuration if exists
  jsonfile = os.path.join(rcPATH,'config.json')
  if os.path.exists(jsonfile):
    config = json.load(open(jsonfile,'r'))
  else:
    print('Nothing to reset: file %s not existing' % os.path.join(rcPATH,'config.json'))
    return
  #--- delete packages in the installation area
  if args.fullreset :
    vprefix = os.path.join(config['PREFIX'],config['VERSION'])
    if os.path.exists(vprefix) : 
      print('Removing packages from %s' % vprefix)
      shutil.rmtree(vprefix)

  print('Reseting current LCGCMAKE session')
  os.remove(jsonfile)

#---Upload-------------------------------------------------------------------------------------------------
def lcgcmake_upload(args):
  jsonfile = os.path.join(rcPATH,'config.json')
  if os.path.exists(jsonfile):
    config = json.load(open(jsonfile,'r'))
  else:
    print('Nothing to upload: file %s not existing' % os.path.join(rcPATH,'config.json'))
    return

  if args.what == 'tarfiles':
    if 'PLATFORM' not in config:
      print('Cannot determine platform')
      return
    platform = config['PLATFORM']
    localdir  = 'tarfiles'
    if args.releaseupload:
      remotedir = '/eos/project/l/lcg/www/lcgpackages/tarFiles/releases'
    else:
      remotedir = '/eos/project/l/lcg/www/lcgpackages/tarFiles/latest'
    print("Uploading all the created binary tarfiles to the EOS repository (%s).\nPassword could be asked by 'scp'" % remotedir)
    tarfiles = []
    patt = re.compile(platform.split('-')[0] + '.*-' + '-'.join(platform.split('-')[1:]))
    if os.path.isdir(localdir):
      for f in os.listdir(localdir):
        if any(p in f for p in ['HEAD', 'master', 'patches']): continue
        if os.path.isfile(os.path.join(localdir, f)) and patt.search(f) : 
          tarfiles.append(os.path.join(localdir, f))

    if not tarfiles:
      print('No tarfiles to upload')
      return

    rc = subprocess.call(['scp']+ tarfiles + ['lxplus.cern.ch:' + remotedir])
    if rc == 0 :
      print("Producing the summary files in the EOS directory.\nPassword could be asked by `ssh`")
      rc = subprocess.call(['ssh','lxplus.cern.ch',os.path.join(remotedir,'make-summaries') + ' ' + platform])
      if rc != 0 :
        sys.exit(rc)
    else:
      sys.exit(rc)

#---Utility functions-----------------------------------------------------------------------------------
def make_compilerversion(command, version):
  patt = re.compile('([0-9]+)[.]([0-9]+)')
  if command.endswith('gcc'):
     mobj = patt.match(version)
     if int(mobj.group(1)) >= 7 :
        compiler = 'gcc' + mobj.group(1)
     else :  
        compiler = 'gcc' + mobj.group(1) + mobj.group(2)
  elif command.endswith('clang'):
     mobj = patt.match(version)
     if int(mobj.group(1)) >= 8 and platform.system() != 'Darwin':
       compiler = 'clang' + mobj.group(1)
     else:  
       compiler = 'clang' + mobj.group(1) + mobj.group(2)
  elif ccommand.endswith('icc'):
     mobj = patt.match(version)
     compiler = 'icc' + mobj.group(1)
  return compiler

def install_tarfile(package, arch, osvers, suffix, prefix):
  #Fix hash separator
  if len(package.split('-')) > 2:
    p, v, h = package.split('-')[:3]
    package2 = p + '-' + v + '_' + h
  else:
    p, v = package.split('-')[:2]
    h = None
    package2 = p + '-' + v
  urltarfile = os.path.join(ctbURL, "-".join([package2, arch, osvers])+suffix+'.tgz')
  print('Downloading and installing %s' % urltarfile)
  try:
    resp = urllib2.urlopen(urltarfile)
    tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
    tar.extractall(path=prefix)
  except (urllib2.HTTPError, urllib2.URLError) as detail:
    print('Error downloading %s : %s' % (urltarfile, detail))
    sys.exit(1)
  except tarfile.ReadError as detail:
    print('Error untaring %s : %s' %(urltarfile, detail))
    sys.exit(1)
  except:
    print('Unexpected error:', sys.exc_info()[0], sys.exc_info[1])
    sys.exit(1)

  # run postinstall
  if h : 
    os.rename(os.path.join(prefix, p, v), os.path.join(prefix, p, v + '-' + h))
    postinstall = os.path.join(prefix, p, v + '-' + h, "-".join([arch, osvers])+suffix,'.post-install.sh')
  else: 
    postinstall = os.path.join(prefix, p, v, "-".join([arch, osvers])+suffix,'.post-install.sh')
  if os.path.exists(postinstall) :
    os.environ['INSTALLDIR'] = prefix
    with open(os.devnull, 'w') as devnull:
      rc = subprocess.call(['/bin/bash',postinstall], stdout=devnull)

def get_available_compilers(arch, osvers):
  infofile = 'LCG_contrib_'+'-'.join([arch, osvers]) + contrib_suffix +'.txt'
  urlinfo = os.path.join(ctbURL, infofile)
  compilers = {}
  try:
    resp = urllib2.urlopen(urlinfo)
    for l in resp.readlines():
      if l[0] == '#' or not l.strip() : continue
      d = parse_info(l)
      if d['NAME'] in ('clang','gcc'):
        compilers[make_compilerversion(d['NAME'],d['VERSION'])] = {'NAME':'-'.join((d['NAME'],d['VERSION'],d['HASH'])),'DEPENDS': d['DEPENDS']}
  except urllib2.HTTPError as e:
    pass
  except:
    print('Unexpected error downloading %s :' % (urlinfo,), sys.exc_info()[0])
    sys.exit(1)
  return compilers

#---Determine the compiler and version--------------------------------
def get_native_compiler():
  system = platform.system()
  if os.getenv('CC'):
    ccommand = os.getenv('CC')
  elif system == 'Windows':
    ccommand = 'cl'
  elif system == 'Darwin':
    ccommand = 'clang'
  else:
    ccommand = 'gcc'

  if ccommand == 'cl':
    versioninfo = str(os.popen(ccommand).read())
    patt = re.compile('.*Version ([0-9]+)[.].*')
    mobj = patt.match(versioninfo)
    compiler = 'vc' + str(int(mobj.group(1))-6)
  elif ccommand.endswith('gcc') or 'gcc' in ccommand.split('/')[-1]:
    versioninfo = str(os.popen(ccommand + ' -dumpversion').read())
    patt = re.compile('([0-9]+)(\\.([0-9]+))?')
    mobj = patt.match(versioninfo)
    if int(mobj.group(1)) >= 7 :
      compiler = 'gcc' + mobj.group(1)
    else :  
      compiler = 'gcc' + mobj.group(1) + mobj.group(3)
  elif ccommand.endswith('clang'):
    versioninfo = str(subprocess.check_output([ccommand,'-v'], stderr=subprocess.STDOUT).decode('utf-8').strip().split('\n')[0].strip()) 
    patt = re.compile('.*version ([0-9]+)[.]([0-9]+)')
    mobj = patt.match(versioninfo)
    if int(mobj.group(1)) >= 8 and platform.system() != 'Darwin':
      compiler = 'clang' + mobj.group(1)
    else:
      compiler = 'clang' + mobj.group(1) + mobj.group(2)
  elif ccommand.endswith('icc'):
     versioninfo = str(os.popen(ccommand + ' -dumpversion').read())
     patt = re.compile('([0-9]+)\\.([0-9]+)')
     mobj = patt.match(versioninfo)
     compiler = 'icc' + mobj.group(1)
  else:
     compiler = 'unk-cmp'
  return compiler

def get_available_versions():
  sourcedir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
  toolchains = os.listdir(os.path.join(sourcedir,'cmake','toolchain'))
  patt = re.compile('heptools-(.*).cmake')
  versions = []
  for tc in toolchains:  
     mobj = patt.match(tc)
     if(mobj) : versions.append(mobj.group(1))
  versions.sort()
  return versions

def get_defined_packages():
  jsonfile = os.path.join(rcPATH,'config.json')
  if os.path.exists(jsonfile):
    config = json.load(open(jsonfile,'r'))
  else:
    print("lcgcmake session has not been configured. Use the command 'lcgcmake configure' first.")
    return []  
  packages = []
  infosfile = 'LCG_%s_%s.txt' % (config['VERSION'], config['PLATFORM'])
  for line in open(infosfile).readlines():
    if line[0] == '#' or line == '\n' : continue
    data = {}
    text = line.strip().split(', ')
    for key, value in [x.split(':') for x in text]:
      data.update({key.strip(): value.strip()})
    deps = [x.split('-')[::2] for x in data['DEPENDS'].split(',') if len(x) != 0]
    packages.append('%s-%s' % (data['NAME'], data['VERSION']))
    packages.sort()
  return packages;

def get_default_version():
  sourcedir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
  branch = subprocess.check_output(['git', 'rev-parse', '--abbrev-ref',  'HEAD'], cwd=sourcedir).strip()
  if branch.startswith('LCG_') and branch[4:] in get_available_versions():
    return branch[4:]
  else:
    return 'hsf'

def parse_info(text):
  data = {}
  text = text.split(', ')
  for key, value in [x.split(':') for x in text]:
    data.update({key.strip(): value.strip()})
  # deps
  if 'VERSION' in data:
    if 'DEPENDS' not in data:
      tmp = data['VERSION'].split(',')
      data['VERSION'] = tmp[0]
      data['DEPENDS'] = [x for x in tmp[1:] if len(x) != 0]
    else:
      data['DEPENDS'] = [x for x in data['DEPENDS'].split(',') if len(x) != 0]
  if 'GITHASH' in data:
    data['GITHASH'] = data['GITHASH'].replace('"', '').replace("'", '')
  return data

def set_env_variable(env, var, val):
  if('PATH' in var):
    if var in env: env[var] += [val]
    else:          env[var]  = [val]
  else:
    env[var] = val

def setup_environment(env):
  for var in env:
    if('PATH' in var):
      if var in os.environ: os.environ[var] = ':'.join(env[var]+[os.environ[var]])
      else:                 os.environ[var] = ':'.join(env[var])
    else:
      os.environ[var] = env[var]

# Code taken from https://gist.github.com/sampsyo/471779
class AliasedSubParsersAction(argparse._SubParsersAction):
  class _AliasedPseudoAction(argparse.Action):
    def __init__(self, name, aliases, help):
      dest = name
      if aliases:
        dest += ' (%s)' % ','.join(aliases)
      sup = super(AliasedSubParsersAction._AliasedPseudoAction, self)
      sup.__init__(option_strings=[], dest=dest, help=help) 
  def add_parser(self, name, **kwargs):
    aliases = kwargs.pop('aliases', [])
    parser = super(AliasedSubParsersAction, self).add_parser(name, **kwargs)
    # Make the aliases work.
    for alias in aliases:
      self._name_parser_map[alias] = parser
    # Make the help text reflect them, first removing old help entry.
    if 'help' in kwargs:
      help = kwargs.pop('help')
      self._choices_actions.pop()
      pseudo_action = self._AliasedPseudoAction(name, aliases, help)
      self._choices_actions.append(pseudo_action)
    return parser

# Local backports 
if sys.version_info < (2,7) :
  def subprocess_check_output(args):
    return subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
  subprocess.check_output = subprocess_check_output
if sys.version_info < (3,3) :
  def which(file):
    for path in os.environ["PATH"].split(os.pathsep):
      filename = os.path.join(path, file)
      if os.path.exists(filename) and os.access(filename, os.X_OK):
        return filename
    return None
  shutil.which = which

if __name__ == '__main__':
  #---default values
  arch = platform.machine()
  system = platform.system()
  if system == 'Darwin' :
     osvers = 'mac' + ''.join(platform.mac_ver()[0].split('.')[:2])
  elif system == 'Linux' :
    dist = platform.linux_distribution()
    if re.search('SLC', dist[0]):
      osvers = 'slc' + dist[1].split('.')[0]
    elif re.search('CentOS', dist[0]):
      osvers = 'centos' + dist[1].split('.')[0]
    elif re.search('Ubuntu', dist[0]):
      osvers = 'ubuntu' + dist[1].split('.')[0] + dist[1].split('.')[1]
    elif re.search('Fedora', dist[0]):
      osvers = 'fedora' + dist[1].split('.')[0]
    else:
      osvers = 'linux' + ''.join(platform.linux_distribution()[1].split('.')[:2])
  elif system == 'Windows':
    osvers = win + platform.win32_ver()[0]
  else:
     osvers = 'unk-os'

  parser = argparse.ArgumentParser(usage=helpstring.format(sys.argv[0]))
  parser.register('action', 'parsers', AliasedSubParsersAction)
  parser.add_argument('--verbose', action='count', dest='verbose_level', help='Increase logging verbosity', default=0)
  parser.add_argument('-q', '--quiet', action='count', dest='quiet_level', help='Decrease logging verbosity', default=0)
  parser.add_argument('-a', '--arch', dest='arch', help='processor architecture', default=arch)
  parser.add_argument('-o', '--os', dest='osvers', help='operating system keyword (e.g. slc6, mac1013, etc.) ', default=osvers)
  subparsers = parser.add_subparsers( dest='command', help='available sub-commands')
  p_v = subparsers.add_parser('version', help='print the version of lcgcmake')
  p_c = subparsers.add_parser('configure', help='configure the lcgcmake session by selecting the software stack version, etc.', aliases=['config', 'conf'])
  p_c.add_argument('--local', dest='local', action='store_true', help='Do not use $HOME to create .lcgcmake folder')
  p_c.add_argument('-c', '--compiler', dest='compiler', help='compiler keyword to be used (e.g. gcc62, gcc7, etc. )', default=None)
  p_c.add_argument('-p', '--prefix', dest='prefix', help='prefix to the final installations', default=None)
  p_c.add_argument('-v', '--version', dest='version', help='LCG software stack version', default=None)
  p_c.add_argument('-o', '--options', dest='options', nargs='+', help='Additional configuration options (e.g. NAME=VALUE)', default=None)
  p_c.add_argument('-y', '--pyversion', dest='pyversion', help='Python version for the LCG software stack', choices=['2','3'], default=None)
  p_c.add_argument('-g', '--debug', dest='debug', action='store_true', help='Install the debug versions of packages')
  p_c.add_argument('-t', '--target_platform', dest='target_platform', help='Force a plaform string (e.g. x86_64-centos7-gcc7-opt)', default=None)
  p_c.add_argument('--lcg-prefix', dest='lcg_prefix', help='Location(s) of existing LCG releases. Used for incremental builds (: separated)')
  p_c.add_argument('--toolchain', dest='toolchain', help='Set an LCG toolchain file. It takes priority to the composed file using the stack version', default=None)
  p_c.add_argument('--build-always', dest='build_always', action='store_true', help='Force to build all local packages', default=None)
  p_c.add_argument('--no-build-always', dest='build_always', action='store_false', help='Do not force to build all local packages', default=None)
  p_c.add_argument('--no-binary', dest='binary', action='store_false', help='Force not using binary installations if they exists')
  p_c.add_argument('--with-tarfiles', dest='withtarfiles', action='store_true', help='Produce the binary tarfiles while building the packages')
  p_c.add_argument('--with-rpms', dest='withrpms', action='store_true', help='Produce the rpms while building the packages')
  p_m = subparsers.add_parser('install', help='Install or build a set of given targets with all their dependencies')
  p_m.add_argument('targets', nargs='*', help='Select the target[s]', default=['top_packages'])
  p_s = subparsers.add_parser('show', help='get information from the lcgcmake session', aliases=['sh'])
  p_s.add_argument('what', help='What to show', choices=['config','targets','compilers','versions','packages'])
  p_r = subparsers.add_parser('run', help='run a command with the just installed software stack')
  p_r.add_argument('runcmd', nargs='*', help='Command to run', default=['/bin/bash'])
  p_t = subparsers.add_parser('reset', help='reset the lcgcmake session')
  p_t.add_argument('--full', dest='fullreset', action='store_true', help='force a full reset by deleting all the locally installed packages')
  p_u = subparsers.add_parser('upload', help='Upload build/install artifacts', aliases=['up'])
  p_u.add_argument('what', help='What to uopload', choices=['tarfiles','rpms'])
  p_u.add_argument('--release', dest='releaseupload', action='store_true', help='Upload into the release directory')
 
  args = parser.parse_args()

  if args.command in ['configure','config','conf'] :
    lcgcmake_configure(args)
  elif args.command in ['install'] :
    lcgcmake_install(args)
  elif args.command in ['show', 'sh'] :
    lcgcmake_show(args)
  elif args.command in ['run'] :
    lcgcmake_run(args)
  elif args.command in ['version'] :
    lcgcmake_version(args)
  elif args.command in ['reset'] :
    lcgcmake_reset(args)
  elif args.command in ['upload', 'up'] :
    lcgcmake_upload(args)
