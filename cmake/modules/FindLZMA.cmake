# - Find lzma
# Find the native LZMA includes and library
#
#  LZMA_INCLUDE_DIR    - where to find lzma.h, etc.
#  LZMA_LIBRARIES      - List of libraries when using liblzma.
#  LZMA_FOUND          - True if liblzma found.

find_path(LZMA_INCLUDE_DIR lzma.h)
find_library(LZMA_LIBRARY NAMES lzma)

# handle the QUIETLY and REQUIRED arguments and set LZMA_FOUND to TRUE if 
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LZMA DEFAULT_MSG LZMA_LIBRARY LZMA_INCLUDE_DIR)

if(LZMA_FOUND)
  set(LZMA_LIBRARIES ${LZMA_LIBRARY})
endif()

mark_as_advanced(LZMA_LIBRARY LZMA_INCLUDE_DIR)
