# Update the summary file in the build area
# Parameters:
#   NAME        for the file will be called buildinfo_<name>.txt
#   SUMFILE     the name of the summary file ${CMAKE_BINARY_DIR}/LCG_${LCG_VERSION}_${LCG_system}.txt
#   INSTALLDIR  installion dir to locate buildinfo_<name>.txt


if(EXISTS ${INSTALLDIR}/.buildinfo_${NAME}.txt)
  #---Read the file -----------------------------------------------------------------------------------------------------------------------
  file(STRINGS ${INSTALLDIR}/.buildinfo_${NAME}.txt buildinfo)
  #---Cope with obsolete formats of .buildinfo file in release area
  string(REPLACE "SVNREVISION:" "DIRECTORY: ${NAME}, SVNREVISION:" buildinfo ${buildinfo})
  string(REPLACE "GITHASH: \"format:" "DIRECTORY: ${NAME}, GITHASH: '" buildinfo ${buildinfo})
  string(REPLACE "\"" "'" buildinfo ${buildinfo})
  if(NOT ${buildinfo} MATCHES "DIRECTORY:")
    string(REPLACE "VERSION:" "DIRECTORY: ${NAME}, VERSION:" buildinfo ${buildinfo})
  endif()
  file(LOCK lock_summary.cmake TIMEOUT 10)
  file(READ ${SUMFILE} summary)
  string(REPLACE "# PLACEHOLDER for package ${NAME}" "${buildinfo}" summary ${summary})
  file(WRITE ${SUMFILE} ${summary})
  file(LOCK lock_summary.cmake RELEASE)
else()
  message("==== .buildinfo_${NAME}.txt file not found")
endif()

