#!/usr/bin/env python

#----------------------------------------------------------------------------------------------
# This script fixes the installed libraries for MacOS in CVMFS. It uses an LCG as starting point.
# Since CVMFS is readonly, then it copies the libraries and binaries to be modified locally.
# Usage:
#    mkdir workdir; cd workdir   # create a work directory
#    fix_mac_relocation.py /cvmfs/sft.cern.ch/lcg/views/<version>/<platform>
#    tar -cvzf ../macfix.tar.gz *
#    scp ../macfix.tar.gz sftnight@lxplus.cern.ch:public/.
#    <in the cvmfs-sft node>
#    cd /cvmfs/sft.cern.ch/lcg/releases
#    tar -xvf ~sftnight/public/macfix.tar.gz 
#----------------------------------------------------------------------------------------------
import os, sys, glob, subprocess, shutil, stat

lcgrelease = '/cvmfs/sft.cern.ch/lcg/releases/'


# Partially borrowed from https://github.com/LLNL/spack 
def fix_darwin_install_name(path):
    """Fix install name of dynamic libraries on Darwin to have full path.
    There are two parts of this task:
    1. Use ``install_name('-id', ...)`` to change install name of a single lib
    2. Use ``install_name('-change', ...)`` to change the cross linking between
       libs. The function assumes that all libraries are in one folder and
       currently won't follow subfolders.
    Parameters:
        path (str): directory in which .dylib files are located
    """
    libs = glob.glob(join_path(path, "lib", "*.dylib")) + glob.glob(join_path(path, "lib", "*", "*.dylib")) + glob.glob(join_path(path, "lib", "*.so")) + glob.glob(join_path(path, "lib", "*", "*", "*.oct")) + glob.glob(join_path(path, "lib", "R", "library", "*", "libs", "*.so"))
    libs = {os.path.realpath(lib) for lib in libs}
    #libs = {lib for lib in libs}
    bins = glob.glob(join_path(path, "bin", "*"))
    bins = {os.path.realpath(bin) for bin in bins if os.path.isfile(bin) and not istextfile(bin)}
    for obj in libs | bins:
        # fix install name first (only for libraries and if not yet done):
        id = subprocess.Popen(["otool", "-D", obj],stdout=subprocess.PIPE).communicate()[0].split('\n')[1]
        if obj in libs and not os.path.islink(obj) and id and id != obj:
            subprocess.Popen(
                ["install_name_tool", "-id", obj, localcopy(obj)],
                stdout=subprocess.PIPE).communicate()[0]
            #print 'install_name_tool -id %s %s'% (obj, localcopy(obj))
        long_deps = subprocess.Popen(
            ["otool", "-L", obj],
            stdout=subprocess.PIPE).communicate()[0].split('\n')
        deps = [dep.partition(' ')[0][1::] for dep in long_deps[1:-1]]
        # fix all dependencies:
        for dep in deps:
            for lib in libs:
                # We really want to check for either
                #     dep == os.path.basename(lib)   or
                #     dep == join_path(builddir, os.path.basename(lib)),
                # but we don't know builddir (nor how symbolic links look
                # in builddir). We thus only compare the basenames.
                if os.path.basename(dep) == os.path.basename(lib) and dep != lib :
                    subprocess.Popen(
                        ["install_name_tool", "-change", dep, lib, localcopy(obj)],
                        stdout=subprocess.PIPE).communicate()[0]
                    #print 'install_name_tool -change %s %s %s' % (dep, lib, localcopy(obj))
                    break

def localcopy(obj):
    if not obj.startswith('/cvmfs/') :
        if not os.access(obj, os.W_OK | os.X_OK) :
            #print('Changing access attributes to file %s' % obj)
            subprocess.call(["chmod", "u+w",obj])
        return obj
    # make a copy locally (CVMFS is read only!)
    lobj = obj.replace(lcgrelease,'')
    ldir = os.path.dirname(lobj)
    if not os.path.isdir(ldir) : os.makedirs(ldir)
    if not os.path.exists(lobj) :
        shutil.copy(obj,ldir)
        os.chmod(lobj, os.stat(lobj).st_mode | stat.S_IWUSR)
    return lobj


PY3 = sys.version_info[0] == 3
# A function that takes an integer in the 8-bit range and returns
# a single-character byte object in py3 / a single-character string
# in py2.
#
int2byte = (lambda x: bytes((x,))) if PY3 else chr
_text_characters = (
        b''.join(int2byte(i) for i in range(32, 127)) +
        b'\n\r\t\f\b')

def istextfile(filename, blocksize=512):
    """ Uses heuristics to guess whether the given file is text or binary,
        by reading a single block of bytes from the file.
        If more than 30% of the chars in the block are non-text, or there
        are NUL ('\x00') bytes in the block, assume this is a binary file.
    """
    fileobj = open(filename)
    block = fileobj.read(blocksize)
    if b'\x00' in block:
        # Files with null bytes are binary
        return False
    elif not block:
        # An empty file is considered a valid text file
        return True

    # Use translate's 'deletechars' argument to efficiently remove all
    # occurrences of _text_characters from the block
    nontext = block.translate(None, _text_characters)
    return float(len(nontext)) / len(block) <= 0.30

def join_path(prefix, *args):
    path = str(prefix)
    for elt in args:
        path = os.path.join(path, str(elt))
    return path

#########################
if __name__ == "__main__":
     path = sys.argv[1]
     fix_darwin_install_name(path)
