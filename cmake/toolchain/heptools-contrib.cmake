#---Contribs-----------------------------------------------------------
LCG_external_package(binutils          2.28                                     )
LCG_external_package(gcc              6.2.0                                     )
LCG_external_package(gcc              7.3.0                                     )
LCG_external_package(gcc              8.1.0                                     )
LCG_external_package(clang            6.0.0           gcc=7.3.0                 )
LCG_external_package(clang            7.0.0           gcc=8.1.0                 )
