#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)
 
LCG_external_package(ROOT    HEAD     GIT=http://root.cern.ch/git/root.git         )
LCG_external_package(hepmc3  HEAD     GIT=https://gitlab.cern.ch/hepmc/HepMC3.git  )
LCG_remove_package(DD4hep)
LCG_AA_project(DD4hep        master   GIT=https://github.com/AIDASoft/DD4hep.git   )
LCG_remove_package(Gaudi)
