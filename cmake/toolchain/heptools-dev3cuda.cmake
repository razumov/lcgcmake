#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

# Downgrade the C++ standard since Cuda (9) does not support c++17
#    This is a bit of hack since packages already build with a given hash value might have been built 
#    a different standard. It is OK for the nightlies for the time being.
if(LCG_CPP17)
  set(LCG_CPP17 FALSE)
  set(LCG_CPP14 TRUE)
endif()

#---Overwrites and additional packages ----------------------------
LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )

LCG_external_package(cuda         10.0                          )
LCG_external_package(cudnn        7.6.1.34                      )

LCG_external_package(appdirs      1.4.3                         )
LCG_external_package(py_tools     2019.1.1                      )
LCG_external_package(pybind11     2.3.0                         )
LCG_external_package(pyopencl     2019.1                        )
LCG_external_package(pycuda       2019.1.1                      )
LCG_external_package(mako         1.0.14                        )
LCG_external_package(cupy         6.2.0                         )
LCG_external_package(fastrlock    0.4                           )

LCG_external_package(rangev3  0.9.1                             )
