#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

#---Additional External packages------(Generators)-----------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-generators.cmake)
 
LCG_external_package(ROOT    v6-18-00-patches  GIT=http://root.cern.ch/git/root.git )
LCG_external_package(hepmc3  3.1.1  )

# Latest tagged version of COOL, CORAL instead of master (if required)
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu18|mac[0-9]+)
  LCG_remove_package(CORAL)
  LCG_AA_project(CORAL 3_2_2)
  LCG_remove_package(COOL)
  LCG_AA_project(COOL 3_2_2)
endif()
