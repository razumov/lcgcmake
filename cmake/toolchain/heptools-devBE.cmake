#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)
 
# We don't need ROOT in devBE but without a defined version CMake fails
LCG_external_package(ROOT   v6-18-00-patches   GIT=http://root.cern.ch/git/root.git )
