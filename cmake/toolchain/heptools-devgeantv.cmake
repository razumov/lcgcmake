#---Base LCG configuration--------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

#---Additional overwrites---------------------------------------------------------
LCG_remove_package(COOL)
LCG_remove_package(CORAL)
LCG_remove_package(oracle)

LCG_external_package(ROOT    6.18.04    )
LCG_external_package(hepmc3  3.0.0      )
LCG_external_package(oracle  18.3.0.0.0 )

LCG_external_package(veccore  HEAD  GIT=https://github.com/root-project/veccore.git   )
LCG_external_package(vecmath  HEAD  GIT=https://github.com/root-project/vecmath.git   )
LCG_external_package(VecGeom  HEAD  GIT=https://gitlab.cern.ch/VecGeom/VecGeom.git    )
LCG_external_package(CASTOR   2.1.13-6  castor                                        )

LCG_top_packages(ROOT xrootd Geant4 hepmc3 umesimd VecGeom Vc veccore vecmath benchmark ninja CMake lcgenv doxygen)
