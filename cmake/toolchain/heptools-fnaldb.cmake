# Externals
LCG_external_package(lcgenv            1.3.5                                    )
LCG_external_package(curl              7.48.0                                   )
LCG_external_package(jsoncpp           1.7.2                                    )
LCG_external_package(postgresql        9.5.1                                    )
