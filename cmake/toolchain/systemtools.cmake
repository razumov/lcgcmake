#---define here the packages to be taken from the shystem for this OS and version
if(${LCG_OS}${LCG_OSVERS} STREQUAL centos7 AND NOT ${LCG_ARCH} STREQUAL i686)

    set(LCG_system_packages bzip2 xz uuid curl motif                 jpeg glib               gmp)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL slc6)

    set(LCG_system_packages          uuid curl motif                 jpeg)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL ubuntu1604)

    set(LCG_system_packages bzip2 xz uuid curl motif pkg_config      jpeg     )

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL ubuntu1804)

    set(LCG_system_packages bzip2 xz uuid curl motif pkg_config zlib jpeg glib flex bison m4 gmp)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL mac1014)

    set(LCG_system_packages               curl)

endif()
