For installation to AFS (LCG_68 release and higher) can be use Jenkins (TODO: this job only supports x86_64 builds for now)
For early releases (before LCG_68) do it yourself  

1.  Go to https://phsft-jenkins.cern.ch/ and select lcg_release_dbg

    Select "Build with parameters" 

    Parameters:

      LCG_VERSION:        LCG release version (e.g. 71, 71root6, ...)

      TARGET:             name of the top level package (e.g. rivet-2.3.0) 

      BUILD_POLICY:       Generators

      LCG_INSTALL_PREFIX: /afs/cern.ch/sw/lcg/releases

      Combinations:       select the necessary platforms 

      VERSION:            LCGCMake branch to use for the build (master, experimental, ... )

    Click build and wait until the build is completed

2.  Select lcg_release_installToAFS on https://phsft-jenkins.cern.ch
    
    Select "Build with parameters"

    Parameters:

      LCG_VERSION:         LCG version to install

      TARGET:              Target to install ("all" for entire release or "generators" for installing new generators)

      RPM_REVISION:        the latest release number from /afs/cern.ch/sw/lcg/tmp/incoming_rpms

      It looks like as:

      LCG_generators_75root6_x86_64_slc6_gcc49_opt-1.0.0-82.noarch.rpm
                                                       ->  <- is release number
